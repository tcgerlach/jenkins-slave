FROM rastasheep/ubuntu-sshd
MAINTAINER Thomas Gerlach <info@talixa.com>

# Install GIT and IA32 libs
RUN apt-get install -y git && \
    apt-get install -y lib32z1 && \
    apt-get install -y lib32stdc++6

# Install Java
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y  software-properties-common && \
    add-apt-repository ppa:webupd8team/java -y && \
    apt-get update && \
    echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
    apt-get install -y oracle-java8-installer && \
    apt-get clean

