About
----
This is a simple Ubuntu image w/ support for SSH (root/root) and Java 8 to be used as a Jenkins slave.  

Base image is rastasheep/ubuntu-sshd  
Added git binaries  
Added ia32 libs (for running 32-bit binaries from mounted volumes)  
Applied Java install scripts  

This image was specifically designed to be used as a slave for Android builds.  
For android builds, add an 'Execute shell' build task with the following command:  
echo sdk.dir=/opt/android/Sdk > local.properties  

Replaceing the path above with the path you mounted when the container was created.

Usage
-----
docker run -d tcgerlach/jenkins-slave

If you need to mount volumes (for android sdk, for example)  
docker run -d -v /home/user/android_sdk:/opt/android_sdk tcgerlach/jenkins-slave

Git Repository
-------------
https://bitbucket.org/tcgerlach/jenkins-slave.git


