# This makefile is for testing the docker build
# clean stops and removes the test instance
# build recreates the docker image
# start starts the docker image for testing
# inspect shows the IP address of the image
# cycle stops the current instance, recreates it, starts, and checks ip
# connect tests connectivity to ssh

clean:
	docker stop android-slave
	docker rm android-slave

build:
	docker build -t slave .

start:
	docker run -d -v /home/thomas/Android:/opt/android --name android-slave slave

inspect:
	docker inspect android-slave | grep IPAddress

cycle: clean build start inspect

connect:
	ssh root@172.17.0.4
